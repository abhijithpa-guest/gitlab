Source: gitlab
Section: net
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>,
 Pirate Praveen <praveen@debian.org>,
 Balasankar C <balasankarc@autistici.org>,
 Sruthi Chandran <srud@disroot.org>
Build-Depends: debhelper (>= 10~), gem2deb, bc
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/ruby-team/gitlab.git
Vcs-Browser: https://salsa.debian.org/ruby-team/gitlab
Homepage: https://about.gitlab.com/
XS-Ruby-Versions: all

Package: gitlab
Section: contrib/net
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${shlibs:Depends}, ${misc:Depends},
 gitlab-common (= ${source:Version}),
 ruby (>= 1:2.5~),
 lsb-base (>= 3.0-6),
 rake (>= 12.3.0~),
 bundler,
 postgresql-client,
 postgresql-contrib,
 dbconfig-pgsql | dbconfig-no-thanks,
 bc,
 redis-server (>= 2:2.8~),
 nodejs (>= 6~),
 nginx | httpd,
 default-mta | postfix | exim4 | mail-transport-agent,
 openssh-client,
 bzip2,
 ucf,
 gitlab-workhorse (>= 7.6~),
 ruby-rails (>= 2:5.2.0~),
 ruby-rails-deprecated-sanitizer (>= 1.0.3~),
 ruby-nakayoshi-fork (>= 0.0.4~),
 ruby-responders (>= 2.0~),
 ruby-sprockets (>= 3.7~),
 ruby-default-value-for (>= 3.1.1~),
#ruby-mysql2 | ruby-pg,
 ruby-pg (>= 1.1~),
 ruby-rugged (>= 0.27~),
 ruby-grape-path-helpers (>= 1.1~),
 ruby-faraday (>= 0.12~),
# Authentication libraries
 ruby-devise (>= 4.4.3~),
 ruby-doorkeeper (>= 4.3~),
 ruby-doorkeeper-openid-connect (>= 1.5~),
 ruby-omniauth (>= 1.8~),
 ruby-omniauth-auth0 (>= 2.0~),
 ruby-omniauth-azure-oauth2 (>= 0.0.10~),
 ruby-omniauth-cas3 (>= 1.1.4~),
 ruby-omniauth-facebook (>= 4.0~),
 ruby-omniauth-github (>= 1.3~),
 ruby-omniauth-gitlab (>= 1.0.2~),
# ruby-omniauth-google-oauth2 (>= 0.6~),
 ruby-omniauth-kerberos (>= 0.3.0-3~),
 ruby-omniauth-oauth2-generic (>= 0.2.2~),
 ruby-omniauth-saml (>= 1.10~),
 ruby-omniauth-shibboleth (>= 1.3~),
 ruby-omniauth-twitter (>= 1.4~),
 ruby-omniauth-crowd (>= 2.2~),
 ruby-omniauth-authentiq (>= 0.3.3~),
 ruby-rack-oauth2 (>= 1.2.1~),
# ruby-jwt (>= 2.1~),
# Spam and anti-bot protection
 ruby-recaptcha (>= 4.11~),
 ruby-akismet (>= 2.0~),
# Two-factor authentication
 ruby-devise-two-factor (>= 3.0~),
 ruby-rqrcode-rails3 (>= 0.1.7~),
 ruby-attr-encrypted (>= 3.1~),
 ruby-u2f (>= 0.2.1~),
# GitLab Pages
 ruby-validates-hostname (>= 1.0.6~),
 ruby-zip (>= 1.2.2),
# Browser detection
 ruby-browser (>= 2.5~),
# GPG
 ruby-gpgme (>= 2.0.18~),
# LDAP Auth
 ruby-omniauth-ldap (>= 2.0.4~),
   ruby-ntlm (>= 0.6.1~),
 ruby-net-ldap (>= 0.16.1~),
# API
 ruby-grape (>= 1.1~),
 ruby-grape-entity (>= 0.7.1~),
 ruby-rack-cors (>= 1.0~),
# GraphQL API
 ruby-graphql (>= 1.8~),
 ruby-graphiql-rails (>= 1.4.10~),
# Disable strong_params so that Mash does not respond to :permitted?
 ruby-hashie-forbidden-attributes,
# Pagination
 ruby-kaminari (>= 1.0~),
# HAML
 ruby-hamlit (>= 2.8.8~),
# Files attachments
 ruby-carrierwave (>= 1.3~),
 ruby-mini-magick,
# for backups
 ruby-fog-aws (>= 3.3~),
 ruby-fog-core (>= 2.1~),
   ruby-excon (>= 0.60~),
 ruby-fog-google (>= 1.8~),
 ruby-fog-local (>= 0.6~),
 ruby-fog-openstack (>= 1.0~),
 ruby-fog-rackspace (>= 0.1.1~),
 ruby-fog-aliyun (>= 0.3~),
# for Google storage
 ruby-google-api-client (>= 0.28~),
   ruby-googleauth (>= 0.8~),
# for aws storage
 ruby-unf (>= 0.1.4-2~),
   ruby-unf-ext (>= 0.0.7.4),
# Seed data
 ruby-seed-fu (>= 2.3.7~),
# Markdown and HTML processing
 ruby-html-pipeline (>= 2.8~),
 ruby-task-list (>= 2.0~),
 ruby-github-markup (>= 1.7~),
 ruby-redcarpet (>= 3.4~),
 ruby-commonmarker (>= 0.17~),
 ruby-redcloth (>= 4.3.2-3~),
# rdoc is built-in with ruby
 ruby-org (>= 0.9.12-2~),
 ruby-creole (>= 0.5.0~),
 ruby-wikicloth (>= 0.8.1~),
 asciidoctor (>= 1.5.8~),
 ruby-asciidoctor-plantuml (>= 0.0.8~),
 ruby-rouge (>= 3.1~),
 ruby-truncato (>= 0.7.11~),
 ruby-bootstrap-form (>= 2.7~),
 ruby-nokogiri (>= 1.10~),
 ruby-escape-utils (>= 1.2.1~),
# Calendar rendering
 ruby-icalendar,
# Diffs
 ruby-diffy (>= 3.1~),
# Application server
# The 2.0.6 version of rack requires monkeypatch to be present in
# `config.ru`. This can be removed once a new update for Rack
# is available that contains https://github.com/rack/rack/pull/1201
 ruby-rack (>= 2.0.6~),
 unicorn (>= 5.4~),
   ruby-kgio (>= 2.11.2~),
 ruby-unicorn-worker-killer (>= 0.4.4~),
# State machine
 ruby-state-machines-activerecord (>= 0.5.1~),
# Issue tags
 ruby-acts-as-taggable-on (>= 6.0~),
# Background jobs
 ruby-sidekiq (>= 5.2.1~),
 ruby-sidekiq-cron (>= 1.0~),
 ruby-redis-namespace (>= 1.6~),
 ruby-gitlab-sidekiq-fetcher (>= 0.4.0~),
# Cron Parser
 ruby-fugit (>= 1.1~),
# HTTP requests
 ruby-httparty (>= 0.15.6~),
# Colored output to console
 ruby-rainbow (>= 3.0~),
# Progress bar
 ruby-progressbar,
# GitLab settings
 ruby-settingslogic (>= 2.0.9~),
# Linear-time regex library for untrusted regular expressions
 ruby-re2 (>= 1.1.1~),
# Misc
 ruby-version-sorter (>= 2.1~),
# Export Ruby Regex to Javascript
 ruby-js-regex (>= 3.1~),
# User agent parsing
 ruby-device-detector,
# Cache
 ruby-redis-rails (>= 5.0.2~),
# Redis
 ruby-redis (>= 3.2~),
 ruby-connection-pool (>= 2.0~),
# Discord integration
 ruby-discordrb-webhooks (>= 3.3~),
# HipChat integration
 ruby-hipchat (>= 1.5~),
# JIRA integration
 ruby-jira (>= 1.4~),
# Flowdock integration
 ruby-flowdock (>= 0.7~),
   ruby-posix-spawn (>= 0.3.13~),
# Slack integration
 ruby-slack-notifier (>= 1.5.1~),
# Hangouts Chat integration
 ruby-hangouts-chat (>= 0.0.5),
# Asana integration
 ruby-asana (>= 0.8.1~),
# FogBugz integration
 ruby-fogbugz (>= 0.2.1-3~),
# Kubernetes integration
 ruby-kubeclient (>= 4.2.2~),
# Sanitize user input
 ruby-sanitize (>= 4.6.5~),
 ruby-babosa (>= 1.0.2~),
# Sanitizes SVG input
 ruby-loofah (>= 2.2~),
# Working with license
 ruby-licensee (>= 8.9~),
# Protect against bruteforcing
 ruby-rack-attack (>= 4.4.1~),
# Ace editor
 ruby-ace-rails-ap (>= 4.1~),
# Detect and convert string character encoding
 ruby-charlock-holmes (>= 0.7.5~),
# Detect mime content type from content
 ruby-mimemagic (>= 0.3.2~),
# Faster blank
 ruby-fast-blank,
# Parse time & duration
 ruby-chronic (>= 0.10.2-3~),
 ruby-chronic-duration (>= 0.10.6~),
#
 ruby-webpack-rails (>= 0.9.10~),
# Many node modules are still in NEW, some are yet to be packaged
# so we use yarn to downlod those and hence gitlab is in contrib
 yarnpkg,
 ruby-rack-proxy (>= 0.6~),
#
 ruby-sass-rails (>= 5.0.6~),
 ruby-sass (>= 3.5~),
 ruby-uglifier (>= 2.7.2~),
   libjs-uglify (<< 3.0~),
#
 ruby-addressable (>= 2.5.2~),
 ruby-font-awesome-rails (>= 4.7~),
 ruby-gemojione (>= 3.3~),
 ruby-gon (>= 6.2~),
 ruby-jquery-atwho-rails (>= 1.3.2~),
 ruby-request-store (>= 1.3~),
 ruby-select2-rails (>= 3.5.9~),
 ruby-virtus (>= 1.0.5-3~),
 ruby-base32 (>= 0.3.0~),
# Sentry integration
 ruby-sentry-raven (>= 2.7~),
#
 ruby-premailer-rails (>= 1.9.7~),
# I18n
 ruby-parser (>= 3.8.2~),
 ruby-rails-i18n (>= 5.1~),
 ruby-gettext-i18n-rails (>= 1.8~),
 ruby-gettext-i18n-rails-js (>= 1.3~),
#
 ruby-batch-loader (>= 1.2.2~),
# Perf bar
 ruby-peek (>= 1.0.1~),
 ruby-peek-gc (>= 0.0.2~),
 ruby-peek-pg (>= 1.3~),
 ruby-peek-rblineprof (>= 0.2.0~),
 ruby-peek-redis (>= 1.2~),
# Metrics
 ruby-method-source (>= 0.8.2-2~),
 ruby-influxdb (>= 0.2~),
# Prometheus
 ruby-prometheus-client-mmap (>= 0.9.4~),
 ruby-raindrops (>= 0.18~),
#
 ruby-octokit (>= 4.9~),
#
 ruby-mail-room (>= 0.9.1~),
#
 ruby-email-reply-trimmer (>= 0.1~),
 ruby-html2text,
#
 ruby-prof (>= 0.17~),
 ruby-rbtrace (>= 0.4~),
# OAuth
 ruby-oauth2 (>= 1.4.1~),
# Health check
 ruby-health-check (>= 2.6~),
# System information
 ruby-vmstat (>= 2.3~),
 ruby-sys-filesystem (>= 1.1.6~),
# SSH host key support
 ruby-net-ssh (>= 1:5.0~),
 ruby-sshkey (>= 1.9~),
# Required for ED25519 SSH host key support
 ruby-ed25519 (>= 1.2~),
 ruby-bcrypt-pbkdf (>= 1.0~),
# Gitaly GRPC client
 ruby-gitaly-proto (>= 1.10~),
 ruby-grpc (>= 1.15~),
 ruby-google-protobuf (>= 3.6~),
#
 ruby-toml-rb (>= 1.0.0-2~),
# Feature toggles
 ruby-flipper (>= 0.13~),
 ruby-flipper-active-record (>= 0.13~),
 ruby-flipper-active-support-cache-store (>= 0.13~),
# Structured logging
 ruby-lograge (>= 0.10~),
 ruby-grape-logging (>= 1.7~),
# Vendored js files
# Keeping this to ease backporting as it is in contrib anyway
# libjs-jquery-atwho,
# libjs-jquery-caret.js,
# libjs-pdf,
# libjs-xterm,
# libjs-jquery-nicescroll,
# libjs-clipboard,
# libjs-chartjs,
# libjs-graphael,
# node-lie,
# node modules - all node packages are stuck in NEW
# using npm for all to ease backporting as it is in contrib anyway
# node-babel-core,
# node-babel-eslint,
# node-babel-loader,
# node-babel-plugin-transform-define,
# node-babel-preset-latest,
# node-babel-preset-stage-2,
# node-bootstrap-sass,
# node-core-js,
# node-d3-array,
# node-d3-axis,
# node-d3-brush,
# node-d3-scale,
# node-d3-selection,
# node-d3-shape,
# node-d3-time,
# node-d3-time-format,
# node-debug (>= 3.1.0~),
# node-exports-loader,
# node-file-loader,
# node-glob,
# node-imports-loader,
# node-jed,
# node-jquery,
# node-js-cookie,
# node-jszip,
# node-jszip-utils,
# node-katex,
# node-marked,
# node-mousetrap,
# node-raw-loader,
# node-stats-webpack-plugin,
# node-underscore,
# node-url-loader,
# node-katex
Recommends: certbot,
 gitaly (>= 1.20~)
Conflicts: libruby2.3
Description: git powered software platform to collaborate on code (non-omnibus)
 gitlab provides web based interface to host source code and track issues.
 It allows anyone for fork a repository and send merge requests. Code review
 is possible using merge request workflow. Using groups and roles project
 access can be controlled.
 .
 Unlike the official package from GitLab Inc., this package does not use
 omnibus.
 .
 Note: Currently this package is in contrib because it uses yarn to install
 front end dependencies.

Package: gitlab-common
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends},
 ruby | ruby-interpreter,
 adduser (>= 3.34~),
 git (>= 1:2.18~),
 ucf,
 gitlab-shell (>= 8.4.4~)
Description: git powered software platform to collaborate on code (common)
 gitlab provides web based interface to host source code and track issues.
 It allows anyone for fork a repository and send merge requests. Code review
 is possible using merge request workflow. Using groups and roles project
 access can be controlled.
 .
 This package includes configurations common to gitlab and gitaly.
