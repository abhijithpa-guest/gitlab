#!/bin/sh

# add gitlab user (requires adduser >= 3.34)
# don't muck around with this unless you KNOW what you're doing

# Take gitlab_user from envornment variable or use gitlab
test -n "${gitlab_user}" || gitlab_user="gitlab"

# Take gitlab_data_dir from envornment variable or use /var/lib/gitlab
test -n "${gitlab_data_dir}" || gitlab_data_dir="/var/lib/gitlab"

# Create gitlab user with home in /var/lib
echo "Creating/updating ${gitlab_user} user account..."
adduser --system --home /var/lib/gitlab --gecos "${gitlab_user} user" --shell /bin/sh \
	--quiet --disabled-password --group ${gitlab_user} || {
echo "Proceeding with existing ${gitlab_user} user..."
  }

# Give gitlab_user ownership of gitlab_data_dir
chown ${gitlab_user} ${gitlab_data_dir}
